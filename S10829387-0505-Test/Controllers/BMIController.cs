﻿using S10829387_0505_Test.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace S10829387_0505_Test.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        }
        // GET: BMI
        [HttpPost]
        public ActionResult Index(BMIData data)
        {
            if ( ModelState.IsValid)
            {
                var Your_height = data.Height/ 100;
                var Result = data.Weight/ (Your_height * Your_height);

                var level = "";
                if (Result < 18.5)
                {
                    level = "體重過輕";
                }
                else if (Result > 18.5 && Result < 24)
                {
                    level = "正常範圍";
                }
                else if (Result > 24 && Result < 27)
                {
                    level = "過重";
                }
                else if (Result > 27 && Result < 30)
                {
                    level = "輕度肥胖";
                }
                else if (Result > 30 && Result < 35)
                {
                    level = "中度肥胖";
                }
                else if (35 <= Result)
                {
                    level = "重度肥胖";
                }

                data.Result = Result;
                data.Level = level;
            }
            return View(data);
        }
    }
}