﻿namespace S10829387_0505_Test.Controllers
{
    public class BMIData
    {
        public float? Weight { get; set; }
        public float? Height { get; set; }
        public float? Result { get; set; }
        public string Level { get; set; }
    }
}