﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace S10829387_0505_Test.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Html()
        {
            return View();
        }
        public ActionResult HtmlHelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
        public ActionResult Index0526()
        {
            ViewData["12345"] = "Hello";
            ViewBag.A = "1";
            ViewBag.B = "2";
            return View();
        }
    }
}